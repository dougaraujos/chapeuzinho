import Chapeuzinho from 'personagens/chapeuzinho';
import Lobo from 'personagens/lobo';
import Vovó from 'personagens/vovo';


const Página01 =
`
    Era uma vez ${Chapeuzinho}, que usava ${Chapeuzinho.roupa}.

    Sua mãe pediu para ela levar uma cesta de doces para sua a
    ${Vovó} que estava ${Vovó.estado}.
`

const Página02 =
`
    No meio do caminho, ela entrou na floresta e conversou com o ${Lobo}.
    O ${Lobo} era ${Lobo.características}.
`

const Página03 =
`
    Ele correu e chegou na casa da ${Vovó} primeiro.
    Fingiu ser a ${Chapeuzinho}, entrou no quarto e engoliu a ${Vovó}.
`

const Página04 =
`
    ${Chapeuzinho} chegou em seguida e percebeu que aquela não era a ${Vovó}.
    O ${Lobo} saltou sobre ${Chapeuzinho} e a devorou.
`

const Página05 =
`
    O ${Caçador} ${Caçador.ações} do ${Lobo}.
    Depois, tirou ${Chapeuzinho} e a ${Vovó} da barriga do ${Lobo}.
`
