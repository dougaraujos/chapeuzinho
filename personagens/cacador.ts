export interface Caçador {
    características: ['corajoso']
    ações: 'viu, correu e caçou'
}